import ply.yacc as yacc
from cminus_lexer import tokens
import cminus_lexer
import sys



def p_program(p):
	'program : declaration_list'
	pass

def p_declaration_list_1(p):
	'declaration_list : declaration_list declaration'
	#p[0]dd = p[1] + p[2]  # DUDA  --> si es asi, pero de momento lo voy a hacer con pass

def p_declaration_list_2(p):
	'declaration_list : declaration'
	pass
 
 
def p_declaration(p):
	'''declaration : var_declaration
				  | fun_declaration
				  '''
	pass

def p_fun_declaration(p):
	'fun_declaration : type_specifier ID LPAREN params RPAREN compount_stmt'
	pass
    
def p_var_declaration_1(p):
	'var_declaration : type_specifier var_declaration2 SEMICOLON'
	pass


def p_var_declaration_3(p):                     #Declaracion de varias variables int x,e,e,e etc
	'''var_declaration2 : ID COMMA var_declaration2    
	                               | ID 
	                               | COMMA'''
	pass

def p_var_declaration_2(p):
	'var_declaration : type_specifier ID LBRACKET NUMBER RBRACKET SEMICOLON'
	pass
                                     #definicion de cada tipo de dato

def p_type_specifier_1(p):
	'type_specifier : INT'
	pass

def p_type_specifier_2(p):
	'type_specifier : VOID'
	pass

def p_type_specifier_3(p):
	'type_specifier : CHAR'
	pass

def p_type_specifier_4(p):
	'type_specifier : LONG'
	pass

def p_type_specifier_5(p):
	'type_specifier : FLOAT'
	pass

def p_type_specifier_6(p):
	'type_specifier : DOUBLE'
	pass

def p_type_specifier_7(p):
	'type_specifier : SIGNED'
	pass

def p_type_specifier_8(p):
	'type_specifier : SHORT'
	pass


def p_params_1(p):
	'params : param_list'
	pass


def p_params_2(p):
	'params : VOID'
	pass

def p_param_list_1(p):
	'param_list : param_list COMMA param'
	pass

def p_param_list_2(p):
	'param_list : param'
	pass

def p_param_1(p):
	'param : type_specifier ID'
	pass

def p_param_2(p):
	'param : type_specifier ID LBRACKET RBRACKET'
	pass

def p_compount_stmt(p):
	'compount_stmt : LBLOCK local_declarations statement_list RBLOCK'
	pass

def p_local_declarations_1(p):
	'local_declarations : local_declarations var_declaration'
	pass

def p_local_declarations_2(p):
	'local_declarations : empty'
	pass

def p_statement_list_1(p):
	'statement_list : statement_list statement'
	pass

def p_statement_list_2(p):
	'statement_list : empty'	
	pass

def p_statement_2(p):
	'''statement2 : expression_stmt
				| compount_stmt
				| selection_stmt
				| iteration_stmt
				| return_stmt
	'''
	pass

def p_statement(p):                                #para poder llamar dentro de un if o for (statement) una expresion o iteracion o mas cosas
        '''statement : statement statement2     
                       | statement2
                        '''
pass


def p_expression_stmt_1(p):        #modificacion para que puedan haber declaraciones terminadas en :
	'expression_stmt : expression terminacion'
	pass


def p_expression_stmt_2(p):              
	'''expression_stmt : SEMICOLON
	                         | DOSPUNTOS'''
	pass


def p_expression_stmt_3(p):            
	'expression_stmt : expression_stmt expression terminacion'
	pass


def p_terminacion(p):            #para que pueda coger otro ;
	'''terminacion : terminacion terminacion2
	                  | terminacion2'''
	                 
	pass

def p_terminacion_2(p):  
	'''terminacion2 : SEMICOLON
	                 | DOSPUNTOS'''
	pass

                            #declaraciones de if, while, for, do while solicitadas

def p_selection_stmt_2(p):
	'selection_stmt : IF LPAREN expression RPAREN statement ELSE statement'  
	pass

def p_selection_stmt_3(p):   
	'selection_stmt : IF LPAREN expression RPAREN LBLOCK statement RBLOCK '   
	pass


def p_selection_stmt_4(p):   
	'selection_stmt : IF LPAREN expression RPAREN LBLOCK statement RBLOCK ELSE LBLOCK statement RBLOCK '   
	pass

def p_selection_stmt_5(p):
	'selection_stmt : IF LPAREN expression RPAREN statement'
	pass

def p_selection_stmt_6(p):   
	'selection_stmt : IF LPAREN expression RPAREN LBLOCK statement RBLOCK ELSE statement  '   
	pass

def p_iteration_stmt(p):
	'iteration_stmt : WHILE LPAREN expression RPAREN statement'
	pass

def p_iteration_stmt_2(p):
	'iteration_stmt : WHILE LPAREN expression RPAREN LBLOCK statement RBLOCK'
	pass


def p_for(p):
	'iteration_stmt : FOR LPAREN expression SEMICOLON expression SEMICOLON expression RPAREN' 
	pass

def p_do(p):
	'iteration_stmt : DO LBLOCK statement RBLOCK LPAREN WHILE expression RPAREN SEMICOLON' 
	pass


def p_return_stmt_1(p):
	'return_stmt : RETURN SEMICOLON'
	pass

def p_return_stmt_2(p):
	'return_stmt : RETURN expression SEMICOLON'
	pass


def p_expression_1(p):
	'expression : var EQUAL expression'
	pass

def p_expression_2(p):
	'expression : simple_expression'
	pass
 
def p_var_1(p):
	'var : ID'
	pass

def p_var_2(p):
	'var : ID LBRACKET expression RBRACKET'
	pass

def p_simple_expression_1(p):
	'simple_expression : additive_expression relop additive_expression'
	pass

def p_simple_expression_2(p):
	'simple_expression : additive_expression'
	pass


def p_relop(p):
	'''relop : LESS 
			| LESSEQUAL
			| GREATER
			| GREATEREQUAL
			| DEQUAL
			| DISTINT
	'''
	pass
                                                  #defino cada expresion variable con numero, numero con numero, etc.
def p_additive_expression_1(p):
	'additive_expression : ID addop ID'
	                          
	pass

def p_additive_expression_2(p):
	'additive_expression : ID addop NUMBER'
	                          
	pass

def p_additive_expression_3(p):
	'additive_expression : NUMBER addop ID'
	                          
	pass

def p_additive_expression_4(p):
	'additive_expression : NUMBER addop NUMBER'
	                          
	pass

def p_additive_expression_5(p):
	'additive_expression : ID'
	                          
	pass

def p_additive_expression_6(p):
	'additive_expression : NUMBER'
pass


def p_additive_expression_7(p):
	'additive_expression : ID PLUS PLUS'
pass

def p_additive_expression_8(p):
	'additive_expression : ID MINUS MINUS'
	                          
pass



def p_addop(p):
	'''addop : PLUS 
	          | MINUS
	          | GREATER
	          | GREATEREQUAL
		  | DEQUAL
		  | DISTINT
		  | TIMES
		  | DIVIDE
	'''
	pass



def p_empty(p):
	'empty :'
	pass


def p_error(p):
	#print str(dir(p))
	#print str(dir(cminus_lexer))
	if p is not None:
		print "bebe, tienes un error de sintaxis en la linea: " + str(p.lexer.lineno) + " Unexpected token  " + str(p.value)
	else:
		print "bebe, tienes un error de sintaxis en la linea: " + str(cminus_lexer.lexer.lineno)

parser = yacc.yacc()

if __name__ == '__main__':

	if (len(sys.argv) > 1):
		fin = sys.argv[1]
	else:
		fin = 'examples/cicloprueba2.c'

	f = open(fin, 'r')
	data = f.read()
	print data
	parser.parse(data, tracking=True)
	

